from serial.serialposix import *
import time


# the chosen baudrate should be a factor of the internal baudrate of each antenna (ideally 1:1)
first = PosixSerial(port='/dev/ttyUSB2', baudrate=57600)
second = PosixSerial(port='/dev/ttyUSB1', baudrate=57600)

def listen_nmea():
    while True:
        res = ''
        a = first.read()
        while a.strip().strip('\n') != '':
            res += a
            a = first.read()
        print(res)

while True:
    first.write("some test")
    print(second.read())



