sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net --recv-key 0xB01FA116
sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 0xB01FA116
sudo apt-get update
sudo apt-get install ros-indigo-desktop-full
sudo rosdep init
rosdep update
echo "source /opt/ros/indigo/setup.bash" >> ~/.bashrc
source ~/.bashrc

cd ~/catkin_ws/src
catkin_init_workspace

echo "export ROS_PACKAGE_PATH="/home/$(whoami)/catkin_ws/src":$ROS_PACKAGE_PATH" >> ~/.bashrc

git clone https://github.com/AutonomyLab/ardrone_autonomy
cd ..
catkin_make
