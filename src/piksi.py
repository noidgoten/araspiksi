#!/usr/bin/env python
import argparse
import time
import os
import signal
from src.qprint import *
import rospy
from geometry_msgs.msg import Point


def signal_handler(signal, frame):
    qprint('\nTrying to stop %s...' % str(piksi), color=BLUE)
    piksi.stop()
    piksi.join()
    qprint('Execution of %s stopped.' % str(piksi), color=GREEN)
    sys.exit(0)


def get_args():  # the args work only with the real piksi, not with the simulation
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--port', nargs=1, default='/dev/ttyUSB0')    # piksi port
    parser.add_argument('--sim', choices=['fixed_rtk', 'float_rtk'])  # replace piksi data with a simulation
    parser.add_argument('--log', action='store_true')                       # whether to log piksi data
    parser.add_argument('--show', action='store_true')                      # whether to stream piksi data to the console
    parser.add_argument('--float_rtk', action='store_true')                 # publish data even in Float RTK
    parser.add_argument('--topic', type=str, default='piksi_rtk')           # name of the topic used for the publishers and subscribers
    args = parser.parse_args()
    if args.log:
        temp = os.path.dirname(os.path.realpath(__file__))
        i = len(temp)
        while temp[i - 1] != '/':
            i -= 1
        args.log_name = temp[:i] + str(time.strftime("%d_%b_%Y_%Hh%Mm%Ss.pog", time.localtime()))  # replaced log with pog because of the p in piksi :O
    return args

if __name__ == '__main__':
    rospy.init_node('piksi')
    publisher = rospy.Publisher('piksi_rtk', Point, queue_size=1)
    time.sleep(1)
    signal.signal(signal.SIGINT, signal_handler)
    args = get_args()
    if args.sim:
        from piksi_sim import Simulation
        piksi = Simulation(args.sim)
    else:
        from piksi_real import Piksi
        piksi = Piksi(args)
    piksi.start()
    while 1:
        time.sleep(0.1)
