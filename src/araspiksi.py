#!/usr/bin/env python
import argparse
import os
import signal
import time
import rospy


from qprint import *


def signal_handler(signal, frame):

    """ smoothly exit the program, shutting down the execution and saving the logs """

    qprint('\nTrying to stop %s...' % str(robot), color=BLUE)
    robot.stop()
    # robot.join()  # in case your robot runs on a thread
    qprint('Execution of %s stopped.' % str(robot), color=GREEN)
    qprint('Trying to stop %s...' % str(piksi), color=BLUE)
    piksi.stop()
    piksi.join()
    qprint('Execution of %s stopped.' % str(piksi), color=GREEN)
    sys.exit(0)


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--port', type=str, default='/dev/ttyUSB0')   # piksi port
    parser.add_argument('--show', action='store_true')                      # stream piksi data to the console
    parser.add_argument('--log', action='store_true')                       # log piksi data
    parser.add_argument('--scorpion', action='store_true')                  # but you can also use it with the scorpion or a simulation
    parser.add_argument('--piksi_sim', choices=['fixed_rtk', 'float_rtk'])  # replace piksi data with a simulation
    parser.add_argument('--move', nargs=2, type=float)                      # move the Robot to (arg1, arg2)
    parser.add_argument('--float_rtk', action='store_true')                 # publish data even in Float RTK (default is to not publish during the float phase)
    parser.add_argument('--topic', type=str, default='piksi_rtk')           # name of the topic used for the publishers and subscribers
    parser.add_argument('--draw', type=str)                                 # the Robot should connect the points provided in the file provided as argument
    parser.add_argument('--scale', type=float, default=1)                   # the points in the provided file are in meters. for half that size, choose scale 0.5
    parser.add_argument('--robosim', action='store_true')                   #
    #parser.add_argument('--reset', action='store_true')                    # reset the drone
    args = parser.parse_args()
    if args.log:
        temp = os.path.dirname(os.path.realpath(__file__))
        i = len(temp)
        while temp[i - 1] != '/':
            i -= 1
        args.log_name = temp[:i] + str(time.strftime("%d_%b_%Y_%Hh%Mm%Ss.log", time.localtime()))
    return args

if __name__ == '__main__':

    rospy.init_node('araspiksi')  # name of the rosnode it will create

    args = get_args()

    """
    the topic to which the Piksi sends its (location only for now) data, and from which the Robot gets its movement task
    it is the only structure that connects a Robot to a Piksi
    """

    time.sleep(1)  # so that ROS has time to create the publisher, before using it later down the execution

    signal.signal(signal.SIGINT, signal_handler)  # manages execution interruption

    if args.piksi_sim:
        from piksi_sim import Simulation
        piksi = Simulation(args)
    else:
        from piksi_real import Piksi
        piksi = Piksi(args)
    piksi.start()

    if args.scorpion:  # choice defaults to Drone (instead of Scorpion, the only 2 options available for now), as intended by the purpose of this library
        from scorpion import Scorpion
        robot = Scorpion(args)  # our Scorpion will be driven by the Piksi
    elif args.robosim:
        from robosim import Robosim
        robot = Robosim(args, piksi)
    else:
        from drone import Drone
        robot = Drone()

    if args.move:
        robot.move(args.move[0], args.move[1])  # moves the Robot to the coordinates provided at launch-time
    elif args.draw:
        robot.draw(args.draw)

    piksi.stop()

