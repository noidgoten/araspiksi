import math
import os
import time
from qprint import *
from rovio.rovio import Rovio
import rospy
from geometry_msgs.msg import Point


class Scorpion(Rovio):
    def __init__(self, args):
        #os.system('sudo ifconfig wlan0 192.168.10.32 netmask 255.255.255.0 up')  # use this unless you correctly configured your adhoc networkt
        #time.sleep(1)
        Rovio.__init__(self, '192.168.10.18')
        self.scale = 1000 * args.scale  # to match the Piksi's position in mm
        self.stopped = False
        self.subscriber = rospy.Subscriber(args.topic, Point, self.update_position)
        self.x = None  # stores the most recent position
        self.y = None  # indicated by the Piksi
        self.pos_updated = False  # set to True iff the Piksi updated the position since beginning the last movement
        self.last_x = None  # stores the position of departure
        self.last_y = None  # for the last movement

    def update_position(self, point):
        # could implement a mean-based update, instead of last position
        self.x = point.x
        self.y = point.y
        self.pos_updated = True

    def __str__(self):
        return 'Scorpion'

    def stop(self):  # shouldn't inherit from Rovio, as it is more of a RovioController...
        self.stopped = True

    def move_forward(self):
        for i in range(15):  # the point is to make the Scorpion advance ~10cm, so the inaccuracy of the GPS doesn't affect the calculations
            self.forward()
            time.sleep(0.1)
        time.sleep(1)

    def rotate(self, angle):
        if angle > 0:
            fnc = self.rotate_left
        else:
            fnc = self.rotate_right
        angle = math.fabs(angle)
        for i in range(int((angle - 1)/15)):  # empirically determined that it takes around 0.1 secs to turn 15 degrees
            fnc(angle=15)
            time.sleep(0.1)

    def move(self, x, y):
        i=0
        qprint("Moving to (%f, %f)" % (x, y))
        while not self.stopped:
            if self.pos_updated:  # check if the Piksi updated the positions (also check if the Piksi emitted any positions yet)

                self.pos_updated = False

                cx = self.x  # current
                cy = self.y  # position

                qprint("Was on (%s, %s). Now on (%f, %f). Target: (%f, %f)." % (self.last_x, self.last_y, cx, cy, x, y))

                if (cx - x) * (cx - x) + (cy - y) * (cy - y) >= 250000:  # if the distance to target is great enough, then move
                    if self.last_x is None:  # either all None, or none None    (meaning the Rovio hasn't started moving yet)
                        self.move_forward()  # initial movement, in a random direction

                    else:
                        """ calculate the needed turn angle (in degrees) """
                        angle = 180 - 180 / math.pi * math.acos(((cx - x) * (cx - self.last_x) + (cy - y) * (cy - self.last_y)) /
                                                    math.sqrt(((cx - x) * (cx - x) + (cy - y) * (cy - y)) * ((cx - self.last_x) * (cx - self.last_x) + (cy - self.last_y) * (cy - self.last_y))))
                        """ turn only if necessary """
                        if angle > 15:
                            qprint("Correcting %f degrees angle." % angle)
                            if (cx - x) * (cy - self.last_y) < (cx - self.last_x) * (cy - y):  # produit vectoriel pointe vers le haut, on doit donc tourner a gauche + changement de direction a cause de (N,E) -> (x,y)
                                self.rotate(angle)
                            else:
                                self.rotate(-angle)
                        else:
                            qprint("Ignoring %f degrees angle." % angle)
                        """ now advance """
                        self.move_forward()

                    """ update positions """
                    self.last_x = cx
                    self.last_y = cy
                else:
                    qprint("Successfully moved to (%f, %f)" % (x, y))
                    return
            else:
                i += 1
                qprint('waiting for rtk')
                time.sleep(1)  # wait for the Piksi to update the current position

    def draw(self, file):
        temp = os.path.dirname(os.path.realpath(__file__))
        i = len(temp)
        while temp[i - 1] != '/':
            i -= 1

        temp = open(temp[:i] + file, 'r')

        for line in temp:
            points = line.strip().split(',')
            self.move(self.scale * int(points[0]), self.scale * int(points[1]))

