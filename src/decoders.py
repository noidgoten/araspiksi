from qprint import *
import struct

translation = {0x100: 'MSG_GPS_TIME', 0x206: 'MSG_DOPS', 0x200: 'MSG_POS_ECEF', 0x201: 'MSG_POS_LLH',
             0x202: 'MSG_BASELINE_ECEF', 0x203: 'MSG_BASELINE_NED', 0x204: 'MSG_VEL_ECEF', 0x205: 'MSG_VEL_NED',
             0xff00: 'MSG_STARTUP', 0xffff: 'MSG_HEARTBEAT', 0x14: 'MSG_ACQ_RESULT', 0xb3: 'MSG_BOOTLOADER_HANDSHAKE_REQ',
             0xb4: 'MSG_BOOTLOADER_HANDSHAKE_RESP', 0xde: 'MSG_NAP_DEVICE_DNA_REQ', 0xdd: 'MSG_NAP_DEVICE_DNA_RESP',
             0x101: 'MSG_EXT_EVENT', 0xa8: 'MSG_FILEIO_READ', 0xa9: 'MSG_FILEIO_READ_DIR_REQ', 0xac: 'MSG_FILEIO_REMOVE',
             0xad: 'MSG_FILEIO_WRITE_REQ', 0xe6: 'MSG_FLASH_PROGRAM', 0xe0: 'MSG_FLASH_DONE', 0xe7: 'MSG_FLASH_READ_REQ',
             0xe1: 'MSG_FLASH_READ_RESP', 0xe2: 'MSG_FLASH_ERASE', 0xe3: 'MSG_STM_FLASH_LOCK_SECTOR',
             0xe4: 'MSG_STM_FLASH_UNLOCK_SECTOR', 0xe8: 'MSG_STM_UNIQUE_ID_REQ', 0xe5: 'MSG_STM_UNIQUE_ID_RESP',
             0x10: 'MSG_PRINT', 0x43: 'MSG_OBS', 0x48: 'MSG_BASE_POS', 0x47: 'MSG_EPHEMERIS', 0xb2: 'MSG_RESET',
             0x23: 'MSG_INIT_BASE', 0x17: 'MSG_THREAD_STATE', 0x18: 'MSG_UART_STATE', 0xa0: 'MSG_SETTINGS_WRITE',
             0xa1: 'MSG_SETTIGNS_SAVE', 0x13: 'MSG_TRACKING_STATE', 'MSG_GPS_TIME': 0x100, 'MSG_DOPS': 0x206,
             'MSG_POS_ECEF': 0x200, 'MSG_POS_LLH': 0x201, 'MSG_BASELINE_ECEF': 0x202, 'MSG_BASELINE_NED': 0x203,
             'MSG_VEL_ECEF': 0x204, 'MSG_VEL_NED': 0x205, 'MSG_STARTUP': 0xff00, 'MSG_HEARTBEAT': 0xffff,
             'MSG_ACQ_RESULT': 0x14, 'MSG_BOOTLOADER_HANDSHAKE_REQ': 0xb3, 'MSG_BOOTLOADER_HANDSHAKE_RESP': 0xb4,
             'MSG_NAP_DEVICE_DNA_REQ': 0xde, 'MSG_NAP_DEVICE_DNA_RESP': 0xdd, 'MSG_EXT_EVENT': 0x101,
             'MSG_FILEIO_READ_REQ': 0xa8, 'MSG_FILEIO_READ_DIR_REQ': 0xa9, 'MSG_FILEIO_REMOVE': 0xac, 'MSG_FILEIO_WRITE_REQ': 0xad,
             'MSG_FLASH_PROGRAM': 0xe6, 'MSG_FLASH_DONE': 0xe0, 'MSG_FLASH_READ_REQ': 0xe7, 'MSG_FLASH_READ_RESP': 0xe1,
             'MSG_FLASH_ERASE': 0xe2, 'MSG_STM_FLASH_LOCK_SECTOR': 0xe3, 'MSG_STM_FLASH_UNLOCK_SECTOR': 0xe4,
             'MSG_STM_UNIQUE_ID_REQ': 0xe8, 'MSG_STM_UNIQUE_ID_RESP': 0xe5, 'MSG_PRINT': 0x10, 'MSG_OBS': 0x43,
             'MSG_BASE_POS': 0x48, 'MSG_EPHEMERIS': 0x47, 'MSG_RESET': 0xb2, 'MSG_INIT_BASE': 0x23, 'MSG_THREAD_STATE': 0x17,
             'MSG_UART_STATE': 0x18, 'MSG_SETTINGS_WRITE': 0xa0, 'MSG_SETTIGNS_SAVE': 0xa1, 'MSG_TRACKING_STATE': 0x13,
             'MSG_IAR_STATE': 0x19, 0x19: 'MSG_IAR_STATE', 0x401: 'MSG_LOG', 'MSG_LOG': 0x401, 0x207: 'MSG_BASELINE_HEADING',
             'MSG_BASELINE_HEADING': 0x207, 0xa4: 'MSG_SETTINGS_READ_REQ', 'MSG_SETTINGS_READ_REQ': 0xa4, 0xa5: 'MSG_SETTINGS_READ_RESP',
             'MSG_SETTINGS_READ_RESP': 0xa5, 0xa2: 'MSG_SETTINGS_READ_BY_INDEX_REQ', 'MSG_SETTINGS_READ_BY_INDEX_REQ': 0xa2,
             0xa7: 'MSG_SETTINGS_READ_BY_INDEX_RESP', 'MSG_SETTINGS_READ_BY_INDEX_RESP': 0xa7, 0xa6: 'MSG_SETTINGS_READ_BY_INDEX_DONE',
             'MSG_SETTINGS_READ_BY_INDEX_DONE': 0xa6, 0xb1: 'MSG_BOOTLOADER_JUMP_TO_APP', 'MSG_BOOTLOADER_JUMP_TO_APP': 0xb1, 0xaa: 'MSG_FILEIO_READ_DIR_RESP',
             'MSG_FILEIO_READ_DIR_RESP': 0xaa, 0xab: 'MSG_FILEIO_WRITE_RESP', 'MSG_FILEIO_WRITE_RESP': 0xab, 0xf3: 'MSG_M25_FLASH_WRITE_STATUS',
             'MSG_M25_FLASH_WRITE_STATUS': 0xf3, 0x69: 'MSG_ALMANAC', 'MSG_ALMANAC': 0x69, 0x68: 'MSG_SET_TIME', 'MSG_SET_TIME': 0x68, 0x22: 'MSG_RESET_FILTERS',
             'MSG_RESET_FILTERS': 0x22, 0x1b: 'MSG_MASK_SATELLITE', 'MSG_MASK_SATELLITE': 0x1b, 0x1c: 'MSG_TRACKING_IQ', 'MSG_TRACKING_IQ': 0x1c, 0x800: 'MSG_USER_DATA',
             'MSG_USER_DATA': 0x800}


def MSG_LOG_decoder(msg):
    MSG = ['EMERG', 'ALERT', 'CRIT', 'ERROR', 'WARN', 'NOTICE', 'INFO', 'DEBUG']
    if msg.msg_type != 0x401:
        qprint("Tried to use MSG_LOG_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        log = ''
        for i in range(len(msg.payload)-1):
            log += struct.unpack('<s', msg.payload[1+i])[0]
        return MSG[int(struct.unpack('<B', msg.payload[:1])[0])], log


def MSG_PRINT_decoder(msg):
    if msg.msg_type != 0x10:
        qprint("Tried to use MSG_PRINT_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return msg.payload.strip('\n')


def MSG_ACQ_RESULT_decoder(msg):
    if msg.msg_type != 0x14:
        qprint("Tried to use MSG_ACQ_RESULT_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<fffHBB', msg.payload)


def MSG_TRACKING_STATE_decoder(msg):
    if msg.msg_type != 0x13:
        qprint("Tried to use MSG_TRACKING_STATE_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        res = []
        n = len(msg.payload) / 9
        for i in range(n):
            res.append(struct.unpack('<BHBBf', msg.payload[i * 9: (i + 1) * 9]))
        return res


def MSG_THREAD_STATE_decoder(msg):
    if msg.msg_type != 0x17:
        qprint("Tried to use MSG_THREAD_STATE_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        res = struct.unpack('<20sHI', msg.payload)
        return res[0].strip('\x00'), res[1], res[2]


def MSG_UART_STATE_decoder(msg):
    if msg.msg_type != 0x18:
        qprint("Tried to use MSG_UART_STATE_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<ffHHBBffHHBBffHHBBiiii', msg.payload)


def MSG_IAR_STATE_decoder(msg):
    if msg.msg_type != 0x19:
        qprint("Tried to use MSG_IAR_STATE_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<I', msg.payload)[0]


def MSG_INIT_BASE_decoder(msg):
    if msg.msg_type != 0x23:
        qprint("Tried to use MSG_INIT_BASE_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return True


def MSG_BASE_POS_ECEF_decoder(msg):
    if msg.msg_type != 0x48:
        qprint("Tried to use MSG_BASE_POS_ECEF_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<ddd', msg.payload)


def MSG_OBS_decoder(msg):
    if msg.msg_type != 0x43:
        qprint("Tried to use MSG_OBS_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        header_t_tow, header_t_wn, header_n_obs = struct.unpack('<IHB', msg.payload[:7])
        n = (len(msg.payload) - 7) / 16
        data = []
        for i in range(n):
            data.append(struct.unpack('<IiBBHHBB', msg.payload[7 + 16 * i: 7 + 16 * (i + 1)]))
        return header_t_tow, header_t_wn, header_n_obs, data


def MSG_EPHEMERIS_decoder(msg):
    if msg.msg_type != 0x47:
        qprint("Tried to use MSG_EPHEMERIS_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<ddddddddddddddddddddHdHBBHBBBHI', msg.payload)


def MSG_SETTINGS_WRITE_decoder(msg):
    if msg.msg_type != 0xa0:
        qprint("Tried to use MSG_SETTINGS_WRITE_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        setting = ''
        for i in range(len(msg.payload)):
            setting += struct.unpack('<s', msg.payload[i])[0]


def MSG_SETTINGS_READ_REQ_decoder(msg):
    if msg.msg_type != 0xa4:
        qprint("Tried to use MSG_SETTINGS_READ_REQ_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        setting = ''
        for i in range(len(msg.payload)):
            setting += struct.unpack('<s', msg.payload[i])[0]


def MSG_SETTINGS_READ_RESP_decoder(msg):
    if msg.msg_type != 0xa5:
        qprint("Tried to use MSG_SETTINGS_READ_RESP_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        setting = ''
        for i in range(len(msg.payload)):
            setting += struct.unpack('<s', msg.payload[i])[0]


def MSG_SETTINGS_READ_BY_INDEX_REQ_decoder(msg):
    if msg.msg_type != 0xa2:
        qprint("Tried to use MSG_SETTINGS_READ_BY_INDEX_REQ_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<H', msg.payload)


def MSG_SETTINGS_READ_BY_INDEX_RESP_decoder(msg):
    if msg.msg_type != 0xa7:
        qprint("Tried to use MSG_SETTINGS_READ_BY_INDEX_RESP_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        setting = ''
        for i in range(len(msg.payload)-2):
            setting += struct.unpack('<s', msg.payload[i+2])[0]
        return struct.unpack('<H', msg.payload[:2])[0], setting


def MSG_SETTINGS_READ_BY_INDEX_DONE_decoder(msg):
    if msg.msg_type != 0xa6:
        qprint("Tried to use MSG_SETTINGS_READ_BY_INDEX_DONE_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return True



def MSG_SETTINGS_SAVE_decoder(msg):
    if msg.msg_type != 0xa1:
        qprint("Tried to use MSG_SETTINGS_SAVE_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return True

def MSG_FILEIO_READ_REQ_decoder(msg):
    if msg.msg_type != 0xa8:
        qprint("Tried to use MSG_FILEIO_READ_REQ_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        file = ''
        for i in range(len(msg.payload)-9):
            file += struct.unpack('<s', msg.payload[i+9])[0]
        return struct.unpack('<IIB', msg.payload), file


def MSG_FILEIO_READ_RESP_decoder(msg):
    if msg.msg_type != 0xa3:
        qprint("Tried to use MSG_FILEIO_READ_RESP_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        contents = []
        for i in range(len(msg.payload)-4):
            contents.append(struct.unpack('<B', msg.payload[i+4]))
        return struct.unpack('<I', msg.payload)[0], contents


def MSG_FILEIO_READ_DIR_REQ_decoder(msg):
    if msg.msg_type != 0xa9:
        qprint("Tried to use MSG_FILEIO_READ_DIR_REQ_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        dir = ''
        for i in range(len(msg.payload)-8):
            dir += struct.unpack('<s', msg.payload[i+8])[0]
        return struct.unpack('<II', msg.payload), dir


def MSG_FILEIO_READ_DIR_RESP_decoder(msg):
    if msg.msg_type != 0xaa:
        qprint("Tried to use MSG_FILEIO_READ_DIR_RESP_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        contents = []
        for i in range(len(msg.payload)-4):
            contents.append(struct.unpack('<B', msg.payload[i+4]))
        return struct.unpack('<I', msg.payload), contents


def MSG_FILEIO_REMOVE_decoder(msg):
    if msg.msg_type != 0xac:
        qprint("Tried to use MSG_FILEIO_REMOVE_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        file = []
        for i in range(len(msg.payload)):
            file.append(struct.unpack('<B', msg.payload[i]))
        return file


def MSG_FILEIO_WRITE_REQ_decoder(msg):  #  NO LONGER WORKING
    if msg.msg_type != 0xad:
        qprint("Tried to use MSG_FILEIO_WRITE_REQ_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        filename, offset = struct.unpack('<20sI', msg.payload[:24])
        data = []
        for i in range(24, len(msg.payload)):
            data.append(struct.unpack('<s', msg.payload[i]))
        return filename, offset, data
    

def MSG_FILEIO_WRITE_RESP_decoder(msg):
    if msg.msg_type != 0xab:
        qprint("Tried to use MSG_FILEIO_WRITE_RESP_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<I', msg.payload)


def MSG_RESET_decoder(msg):
    if msg.msg_type != 0xb2:
        qprint("Tried to use MSG_RESET_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return True


def MSG_BOOTLOARDER_HANDSHAKE_REQ_decoder(msg):
    if msg.msg_type != 0xb3:
        qprint("Tried to use MSG_BOOTLOARDER_HANDSHAKE_REQ_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return True


def MSG_BOOTLOARDER_HANDSHAKE_RESP_decoder(msg):
    if msg.msg_type != 0xb4:
        qprint("Tried to use MSG_BOOTLOARDER_HANDSHAKE_RESP_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        version = ''
        for i in range(4, len(msg.payload)):
            version += struct.unpack('<s', msg.payload[i])[0]
        return struct.unpack('<I', msg.payload[:4])[0], version


def MSG_BOOTLOARDER_JUMP_TO_APP_decoder(msg):
    if msg.msg_type != 0xb3:
        qprint("Tried to use MSG_BOOTLOARDER_JUMP_TO_APP_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<B', msg.payload)


def MSG_NAP_DEVICE_DNA_REQ_decoder(msg):
    if msg.msg_type != 0xde:
        qprint("Tried to use MSG_NAP_DEVICE_DNA_REQ_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return True


def MSG_NAP_DEVICE_DNA_RESP_decoder(msg):
    if msg.msg_type != 0xdd:
        qprint("Tried to use MSG_NAP_DEVICE_DNA_RESP_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<8B', msg.payload)  # might eventually need dividing by 2^7


def MSG_FLASH_DONE_decoder(msg):
    if msg.msg_type != 0xe0:
        qprint("Tried to use MSG_FLASH_DONE_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<B', msg.payload)


def MSG_FLASH_READ_RESP_decoder(msg):
    if msg.msg_type != 0xe1:
        qprint("Tried to use MSG_FLASH_READ_RESP_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<B3BB', msg.payload)


def MSG_FLASH_ERASE_decoder(msg):
    if msg.msg_type != 0xe2:
        qprint("Tried to use MSG_FLASH_ERASE_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<BI', msg.payload)


def MSG_STM_FLASH_LOCK_SECTOR_decoder(msg):
    if msg.msg_type != 0xe3:
        qprint("Tried to use MSG_STM_FLASH_LOCK_SECTOR_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<I', msg.payload)


def MSG_STM_FLASH_UNLOCK_SECTOR_decoder(msg):
    if msg.msg_type != 0xe4:
        qprint("Tried to use MSG_STM_FLASH_UNLOCK_SECTOR_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<I', msg.payload)


def MSG_STM_UNIQUE_ID_RESP_decoder(msg):
    if msg.msg_type != 0xe5:
        qprint("Tried to use MSG_STM_UNIQUE_ID_RESP_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<12B', msg.payload)


def MSG_FLASH_PROGRAM_decoder(msg):
    if msg.msg_type != 0xe6:
        qprint("Tried to use MSG_FLASH_PROGRAM_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        target, addr_start, addr_len = struct.unpack('<B3BB', msg.payload[:5])
        data = []
        for i in range(5, len(msg.payload)):
            data.append(struct.unpack('<B', msg.payload[i]))
        return target, addr_start, addr_len, data


def MSG_FLASH_REQ_decoder(msg):
    if msg.msg_type != 0xe7:
        qprint("Tried to use MSG_FLASH_REQ_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<B3BB', msg.payload)


def MSG_STM_UNIQUE_ID_REQ_decoder(msg):
    if msg.msg_type != 0xe8:
        qprint("Tried to use MSG_STM_UNIQUE_ID_REQ_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return True


def MSG_M25_FLASH_WRITE_STATUS_decoder(msg):
    if msg.msg_type != 0xf3:
        qprint("Tried to use MSG_M25_FLASH_WRITE_STATUS_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<B', msg.payload)


def MSG_GPS_TIME_decoder(msg):
    if msg.msg_type != 0x100:
        qprint("Tried to use MSG_GPS_TIME_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<HIiB', msg.payload)


def MSG_EXT_EVENT_decoder(msg):
    if msg.msg_type != 0x101:
        qprint("Tried to use MSG_EXT_EVENT_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<HIiBB', msg.payload)


def MSG_POS_ECEF_decoder(msg):  # ABSOLUTE POSITIONING
    if msg.msg_type != 0x200:
        qprint("Tried to use MSG_POS_ECEF_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<IdddHBB', msg.payload)


def MSG_POS_LLH_decoder(msg):  # ABSOLUTE POSITIONING
    if msg.msg_type != 0x201:
        qprint("Tried to use MSG_POS_LLH_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<IdddHHBB', msg.payload)


def MSG_BASELINE_ECEF_decoder(msg):  # RELATIVE POSITIONING
    if msg.msg_type != 0x202:
        qprint("Tried to use MSG_BASELINE_ECEF_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<IiiiHBB', msg.payload)


def MSG_BASELINE_NED_decoder(msg):  # RELATIVE POSITIONING
    if msg.msg_type != 0x203:
        qprint("Tried to use MSG_BASELINE_NED_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<IiiiHHBB', msg.payload)


def MSG_VEL_ECEF_decoder(msg):  # ABSOLUTE?
    if msg.msg_type != 0x204:
        qprint("Tried to use MSG_VEL_ECEF_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<IiiiHBB', msg.payload)


def MSG_VEL_NED_decoder(msg):   # ABSOLUTE?
    if msg.msg_type != 0x205:
        qprint("Tried to use MSG_VEL_NED_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<IiiiHHBB', msg.payload)

def MSG_BASELINE_HEADING(msg):   # ABSOLUTE?
    if msg.msg_type != 0x207:
        qprint("Tried to use MSG_BASELINE_HEADING_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<IIBB', msg.payload)


def MSG_DOPS_decoder(msg):
    if msg.msg_type != 0x206:
        qprint("Tried to use MSG_DOPS_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<IHHHHH', msg.payload)


def MSG_STARTUP_decoder(msg):
    if msg.msg_type != 0xff00:
        qprint("Tried to use MSG_STARTUP_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<I', msg.payload)[0]


def MSG_HEARTBEAT_decoder(msg):
    if msg.msg_type != 0xffff:
        qprint("Tried to use MSG_HEARTBEAT_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<I', msg.payload)[0]


def MSG_ALMANAC_decoder(msg):
    if msg.msg_type != 0x69:
        qprint("Tried to use MSG_ALMANAC_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return True


def MSG_SET_TIME_decoder(msg):
    if msg.msg_type != 0x68:
        qprint("Tried to use MSG_SET_TIME_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return True


def MSG_RESET_FILTERS_decoder(msg):
    if msg.msg_type != 0x22:
        qprint("Tried to use MSG_RESET_FILTERS_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<B', msg.payload)[0]


def MSG_MASK_SATELLITE_decoder(msg):
    if msg.msg_type != 0x22:
        qprint("Tried to use MSG_RESET_FILTERS_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        return struct.unpack('<BHBB', msg.payload)


def MSG_TRACKING_IQ_decoder(msg):
    if msg.msg_type != 0x1c:
        qprint("Tried to use MSG_TRACKING_IQ_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        res = []
        channel, ss, sb, sc = struct.unpack('<BHBB', msg.payload)
        res.extend([channel, ss, sb, sc])
        n = (len(msg.payload) - 5) / 8
        for i in range(n):
            res.append(struct.unpack('<ii', msg.payload[5 + i * 8: 5 + (i + 1) * 8]))
        return res

def MSG_USER_DATA_decoder(msg):
    if msg.msg_type != 0x22:
        qprint("Tried to use MSG_USER_DATA_decoder with a %s message." % translation[msg.msg_type], color=RED)
    else:
        contents = []
        for i in range(len(msg.payload)):
            contents.append(struct.unpack('<B', msg.payload[i]))