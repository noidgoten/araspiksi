from threading import Thread
import time
from random import random
from geometry_msgs.msg import Point
import rospy


class Simulation(Thread):
    def __init__(self, args):
        Thread.__init__(self)
        self.publisher = rospy.Publisher(args.topic, Point, queue_size=1)
        self.stopped = False
        self.type = args.piksi_sim
        self.x = 0
        self.y = 0
        self.z = 0

    def stop(self):
        self.stopped = True

    def simulate(self, x, y):
        self.x = x
        self.y = y

    def run(self):
        while not self.stopped:
            if self.type == 'fixed_rtk':
                factor = 50
            else:
                factor = 1000
            self.x += factor * (random() - 0.5)
            self.y += factor * (random() - 0.5)
            self.z += factor * (random() - 0.5)
            self.publisher.publish(Point(self.x, self.y, self.z))
            time.sleep(0.02)

    def __str__(self):
        return 'Piksi simulation'
