from piksi_tools import serial_link
import sbp.client.handler
from threading import Thread
from decoders import *
import os
import time
import rospy
from geometry_msgs.msg import Point

assigned_colors = {}
available_colors = []


def get_color(msg_type):
    """
    function used to assign different console colors (from those available) to different message types
    """
    global assigned_colors, available_colors
    if msg_type not in assigned_colors:
        if not available_colors:
            available_colors = AVAILABLE_COLORS[:]  # defined in qprint.py
        assigned_colors[msg_type] = available_colors.pop()
    return assigned_colors[msg_type]


class Piksi(Thread):
    def __init__(self, args):
        Thread.__init__(self)
        self.fixed_rtk_established = False
        self.last_timestamp = None  # in seconds
        self.satellites = []
        self.args = args
        self.log = None
        self.publisher = rospy.Publisher(args.topic, Point, queue_size=1)
        self.port = args.port
        self.driver = serial_link.get_driver(port=self.port)
        self.handler = sbp.client.handler.Handler(self.driver.read, self.driver.write)

    def msg_handler(self, msg):

        msg_name = translation[msg.msg_type]  # string representing the name of the message

        msg_decoded = globals()[msg_name + '_decoder'](msg)  # decodes the payload using the appropriate decoder

        tail = ''  # small string that will be added at the end of the generated message (mainly for Float/Fixed RTK usage)

        """ chose to use the names of the messages instead of their hex code because the latter are prone to modification"""
        if msg_name in ["MSG_BASELINE_ECEF", "MSG_BASELINE_NED", "MSG_BASELINE_HEADING"]:
            if msg_decoded[-1] & 1 == 1:
                tail = ' | in FIXED RTK'
            else:
                tail = ' | in Float RTK'

        if msg_name == "MSG_POS_LLH":
            if msg_decoded[-1] & 3 == 2:
                tail = ' | in Float RTK'
            elif msg_decoded[-1] & 3 == 1:
                tail = ' | in FIXED RTK'
            else:  # corresponding to msg_decoded[-1] & 3 == 0 (check sbp.pdf)
                tail = ' | in SPP'

        if msg_name == "MSG_POS_ECEF":
            if msg_decoded[-1] & 3 == 2:
                tail = ' | in Float RTK'
            elif msg_decoded[-1] & 3 == 1:
                tail = ' | in FIXED RTK'
            else:
                tail = ' | in SPP'

        if self.args.log:
            self.log.write(str(msg_name) + ': ' + str(msg_decoded) + tail + '\n')

        if self.args.show:
            qprint(str(msg_name) + ': ' + str(msg_decoded) + tail, color=get_color(msg.msg_type))

        msgs = []

        if msg_name == "MSG_OBS":
            current_satellites = [int(x[-3]) + 1 for x in msg_decoded[3]]
            for satellite in current_satellites:
                if satellite not in self.satellites:
                    self.satellites.append(satellite)
                    msgs.append('Satellite %d added.' % satellite)
            for satellite in self.satellites:
                if satellite not in current_satellites:
                    self.satellites.remove(satellite)
                    msgs.append('Satellite %d removed.' % satellite)
            msgs.append(str(self.satellites.sort()))

        for line in msgs:
            if self.args.log:
                self.log.write(line + '\n')
            if self.args.show:
                qprint(line, color=BLUE)

        if msg_name == "MSG_BASELINE_NED":

            fixed_rtk = msg_decoded[-1] & 1 == 1

            if fixed_rtk or self.args.float_rtk:  # Piksi publishes if in Fixed RTK or forced (even Float RTK) mode
                self.publisher.publish(Point(msg_decoded[1], msg_decoded[2], msg_decoded[3]))

            if fixed_rtk and (not self.fixed_rtk_established):  # deal with output messages for Fixed RTK status
                self.fixed_rtk_established = True
                time_needed = int(time.time() - self.last_timestamp)
                self.last_timestamp = time.time()
                if self.args.log:
                    self.log.write('FIXED RTK established after %d minutes and %d seconds.\n' % ((int(time_needed / 60)), time_needed % 60))
                if self.args.show:
                    qprint('FIXED RTK established after %d minutes and %d seconds.' % ((int(time_needed / 60)), time_needed % 60), color=GREEN+BOLD)
            else:
                if self.fixed_rtk_established:
                    time_lasted = int(time.time() - self.last_timestamp)
                    if self.args.log:
                        self.log.write('Lost FIXED RTK after %d minutes and %d seconds.\n' % ((int(time_lasted / 60)), time_lasted % 60))
                    if self.args.show:
                        qprint('Lost FIXED RTK after %d minutes and %d seconds.' % ((int(time_lasted / 60)), time_lasted % 60), color=YELLOW+BOLD)
                    self.fixed_rtk_established = False
                    self.last_timestamp = time.time()

    def run(self):
        self.last_timestamp = time.time()
        self.handler.start()
        qprint("Successfully started Piksi on: %s" % self.port, color=GREEN)
        if self.args.log:
            self.log = open(self.args.log_name, 'w')
            qprint('Log file: %s/%s' % (os.getcwd(), self.args.log_name), color=BLUE)
        self.handler.add_callback(self.msg_handler, None)  # adds self.msg_handler as callback for all message types, the callback will be executed each time a message is received
        self.handler.receive_thread.join()  # to keep the thread alive until execution of the handler thread finishes

    def stop(self):
        self.handler.stop()  # small bypass to be able to shutdown execution without any issues

    def __str__(self):
        return 'Piksi on %s' % str(self.port)
