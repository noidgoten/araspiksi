import math
import os
import time
from qprint import *
from rovio.rovio import Rovio
import random
import rospy
from geometry_msgs.msg import Point
import Tkinter


class Robosim:
    def __init__(self, args, piksi):
        self.root = Tkinter.Tk()
        self.root.title("RTK sim")
        self.widget = Tkinter.Canvas(self.root)
        self.height = 600
        self.width = 600
        self.widget.configure(width=self.width, height=self.height)
        self.widget.pack(fill="both", expand=1)
        self.widget.create_line((self.width/2, 0, self.width/2, self.height), fill="red")
        self.widget.create_line((0, self.height/2, self.width, self.height/2), fill="red")
        self.stopped = False
        self.subscriber = rospy.Subscriber(args.topic, Point, self.update_position)
        self.piksi = piksi
        self.x = None  # stores the most recent position
        self.y = None  # indicated by the Piksi
        self.angle = 360 * (random.random() - 0.5)
        self.pos_updated = False  # set to True iff the Piksi updated the position since beginning the last movement
        self.last_x = None  # stores the position of departure
        self.last_y = None  # for the last movement


    def plot(self, x1, y1, x2, y2):
        self.widget.create_line((self.width / 2 + x1 * self.width / 20000, self.height / 2 - y1 * self.height / 20000 , self.width / 2 + x2 * self.width / 20000, self.height / 2 - y2 * self.height / 20000), fill="black")
        self.widget.update()

    def update_position(self, point):
        # could implement a mean-based update, instead of last position
        self.x = point.x
        self.y = point.y
        self.pos_updated = True

    def __str__(self):
        return 'Robosim'

    def stop(self):  # shouldn't inherit from Rovio, as it is more of a RovioController...
        self.stopped = True

    def move_forward(self):
        self.piksi.simulate(self.x + 100 * math.cos(self.angle * math.pi / 180), self.y + 100 * math.sin(self.angle * math.pi / 180))

    def rotate(self, angle):
        self.angle += angle
        if self.angle > 180:
            self.angle -= 360
        if self.angle < -180:
            self.angle += 360

    def move(self, x, y):
        qprint("Moving to (%f, %f)" % (x, y))
        while not self.stopped:
            if self.pos_updated:  # check if the Piksi updated the positions (also check if the Piksi emitted any positions yet)

                self.pos_updated = False

                cx = self.x  # current
                cy = self.y  # position

                qprint("Was on (%s, %s). Now on (%f, %f). Target: (%f, %f)." % (self.last_x, self.last_y, cx, cy, x, y))


                if (cx - x) * (cx - x) + (cy - y) * (cy - y) >= 22500:  # if the distance to target is great enough, then move
                    if self.last_x is None:  # either all None, or none None    (meaning the Rovio hasn't started moving yet)
                        self.move_forward()  # initial movement, in a random direction

                    else:
                        self.plot(self.last_x, self.last_y, cx, cy)
                        """ calculate the needed turn angle (in degrees) """
                        angle = 180 - 180 / math.pi * math.acos(((cx - x) * (cx - self.last_x) + (cy - y) * (cy - self.last_y)) /
                                                    math.sqrt(((cx - x) * (cx - x) + (cy - y) * (cy - y)) * ((cx - self.last_x) * (cx - self.last_x) + (cy - self.last_y) * (cy - self.last_y))))
                        """ turn only if necessary """
                        if angle > 15:
                            qprint("Correcting %f degrees angle." % angle)
                            if (cx - x) * (cy - self.last_y) > (cx - self.last_x) * (cy - y):  # produit vectoriel pointe vers le haut, on doit donc tourner a gauche
                                self.rotate(angle)
                            else:
                                self.rotate(-angle)
                        else:
                            qprint("Ignoring %f degrees angle." % angle)
                        """ now advance """
                        self.move_forward()

                    """ update positions """
                    self.last_x = cx
                    self.last_y = cy
                else:
                    qprint("Successfully moved to (%f, %f)" % (x, y))
                    return
            else:
                ()
                #qprint('waiting for rtk')
                #time.sleep(1)  # wait for the Piksi to update the current position

    def draw(self, file):
        temp = os.path.dirname(os.path.realpath(__file__))
        i = len(temp)
        while temp[i - 1] != '/':
            i -= 1

        temp = open(temp[:i] + file, 'r')

        for line in temp:
            points = line.strip().split(',')
            self.move(1000*int(points[0]), 1000*int(points[1]))

