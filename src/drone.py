#!/usr/bin/env python
import math
import time

import roslaunch
import rospy
from geometry_msgs.msg import Point
from geometry_msgs.msg import Twist
from std_msgs.msg import Empty

from robot import *


if __name__ == "__main__":
    rospy.init_node("Drone")


class Drone(Robot):
    def __init__(self):
        Robot.__init__(self, 0.005)
        self.x = None
        self.y = None
        self.z = None
        self.last_x = None
        self.last_y = None
        self.last_z = None
        self.node = roslaunch.core.Node('ardrone_autonomy', 'ardrone_driver')
        self.launch = roslaunch.scriptapi.ROSLaunch()
        self.launch.start()
        self.process = self.launch.launch(self.node)
        self.land_topic = rospy.Publisher('/ardrone/land', Empty, queue_size=1)
        self.takeoff_topic = rospy.Publisher('/ardrone/takeoff', Empty, queue_size=1)
        self.reset_topic = rospy.Publisher('/ardrone/reset', Empty, queue_size=1)
        self.twist_topic = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
        self.subscriber = rospy.Subscriber('piksi_rtk', Point, self.update_position)
        time.sleep(1)  # 'cause if you call self.topic too soon, it will not have been created yet

    def __str__(self):
        return 'Drone'

    def shutdown(self):
        self.initialize_tasks(['land'])

    def stop(self):
        self.process.stop()
        self.stopped = True

    def perform_move(self, task):
        if task.__class__.__name__ == 'str':
            if task == 'land':
                #if '/ardrone_driver' in rosnode.get_node_names():  # still to check whether that's the right name
                self.land_topic.publish(Empty())
                time.sleep(2)  # should fine tune this
                qprint("Drone now on the ground.", color=BLUE)

            if task == 'takeoff':
                self.takeoff_topic.publish(Empty())
                time.sleep(3)  # fine tune this
                qprint("Drone now airbrone.", color=BLUE)

            if task == 'reset':
                self.reset_topic.publish(Empty())
                time.sleep(2)  # fine tune this
                qprint("Drone now reset.", color=BLUE)

        if task.__class__.__name__ == 'lock':
            task.release()
            qprint("Lock unlocked.", color=BLUE)

        if task.__class__.__name__ == 'Twist':
            qprint("Executing Twist: %s" % str(task).replace('\n', ' '), color=BLUE)
            self.twist_topic.publish(task)
            time.sleep(self.timeout)

    def update_position(self, point):
        self.x = point.x
        self.y = point.y

    def initialize(self):
        # make it take off
        ()

    def forward(self):
        # move the drone forwards
        ()

    def rotate(self, speed):
        # rotate the drone (+ being to the left)
        ()

    def ascend(self, speed):
        # make the drone ascend (+ being up)
        ()

    def move(self, x, y, z):
        while not self.stopped:
            self.initialize()
            if self.x is None:
                pass
            else:
                if (self.x - x) * (self.x - x) + (self.y - y) * (self.y -y) + (self.z - z) * (self.z - z) >= 10000:
                    if self.last_x is None:
                        self.forward()
                        time.sleep(0.1)
                    else:
                        angle = 180 - 180 / math.pi * math.acos(((self.x - x) * (self.x - self.last_x) + (self.y - y) * (self.y - self.last_y)) /
                                                    math.sqrt(((self.x - x) * (self.x - x) + (self.y - y) * (self.y -y)) * ((self.x - self.last_x) * (self.x - self.last_x) + (self.y - self.last_y))))
                        if angle >= 15:
                            if (self.last_x - x) * (self.y - y) + (self.x - x) * (self.last_y - y) < 0:  # produit vectoriel pointe vers le bas, donc on doit tourner a droite
                                self.rotate(-angle/180)
                            else:
                                self.rotate(angle/180)
                            time.sleep(0.1)
                        self.forward()
                        time.sleep(0.1)
                        if math.fabs(self.z - z) > 50:
                            self.ascend((z - self.z)/50)
                    self.last_x = self.x
                    self.last_y = self.y
                    self.last_z = self.z

