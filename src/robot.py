from threading import Thread, Lock
from collections import deque
from qprint import *


class Robot(Thread):
    def __init__(self, timeout=0):
        Thread.__init__(self)
        self.timeout = timeout
        self.tasks = deque()
        self.run_lock = Lock()  # locked when self.tasks is empty, unlocked otherwise
        self.tasks_lock = Lock()  # locked when something is modifying self.tasks, unlocked otherwise
        self.stopped = False

    def __str__(self):
        return 'Robot'

    def initialize(self):  # instructions to execute to get the robot into the expected state
        pass

    def shutdown(self):  # instructions to execute before stopping the robot
        pass

    def add_tasks(self, tasks):  # extend (to the right) the list of instructions to execute
        if not self.stopped:
            self.tasks_lock.acquire()
            if tasks.__class__.__name__ != 'list':  # in case there's only one instruction -> note that the instructions cannot be of 'list' type
                tasks = [tasks]
            self.tasks.extend(tasks)
            if len(tasks) > 0 and (not self.run_lock.acquire(False)):  # unblock if...
                self.run_lock.release()
            self.tasks_lock.release()

    def initialize_tasks(self, tasks):
        self.tasks_lock.acquire()
        self.tasks = deque(tasks)
        if len(tasks) > 0 and (not self.run_lock.acquire(False)):
            self.run_lock.release()
        self.tasks_lock.release()

    def reset_tasks(self):
        self.initialize_tasks([])

    def get_next_move(self):
        self.tasks_lock.acquire()
        next_move = self.tasks.popleft()
        if self.tasks and (not self.run_lock.acquire(False)):
            self.run_lock.release()
        self.tasks_lock.release()
        return next_move

    def perform_move(self, move):
        pass

    def reset(self):
        pass

    def stop(self):
        self.shutdown()
        self.stopped = True

    def run(self):
        self.initialize()
        while not self.stopped:
            self.run_lock.acquire()
            self.tasks_lock.acquire()
            self.perform_move(self.get_next_move())
            self.tasks_lock.release()
        while self.tasks:
            qprint("Emptying the action queue..", color=BLUE)
            self.perform_move(self.get_next_move())
        qprint("%s execution ended." % str(self), color=BLUE)