#!/bin/bash

git clone https://github.com/swift-nav/piksi_tools.git ~/piksi_tools
cd ~/piksi_tools
export LC_ALL=C
sudo apt -y install libx11-dev libgl1-mesa-dev libglu1-mesa-dev
make deps
sudo apt remove python-requests
sudo python setup.py install

