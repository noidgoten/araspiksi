# Main library

* **Main objects**:

    * **Robot** (robot.py) from which inherit all drones.
        * Drone (drone.py) launches its own ardrone_autonomy rosnode
        * Scorpion (scorpion.py) used to control the Scorpion robot
        * Simulation (robosim.py) used to simulate a correctly moving robot

    * **Real Piksi** (piksi_real.py) used to receive data from the Piksi
    * **Simulated Piksi** (piksi_sim.py) used to generate Piksi-like data. Can be used in fixed_rtk or float_rtk mode.

    * **Piksi rosnode** (piksi.py) used to solely launch the Piksi as a ROS node.

* decoders.py - used to decode the message payload sent by the Piksi. Uses sbp_0.52 specifications

    * **Usage** To run araspiksi as a rosnode: rosrun araspiksi araspiksi.py args, where the valid args are defined and explained in the araspiksi.py file.

* qprint - useful for priting data

* Files:
    * M.txt - file used to draw an M with the drone
    * EMN.txt - file used to draw E M N with the drone

    * To be used with the **--draw** argument when launching the araspiksi node

* **Rovio library**: needed to control the Scorpion

* Scripts:
Use antennas_test.py to check if two antennas are compatible (use the same frequency).

# Installation
 
* sudo apt -y install git
* mkdir -p ~/catkin_ws/src
* git clone https://gitlab.com/noidgoten/araspiksi ~/catkin_ws/src/araspiksi
* cd ~/catkin_ws/src/araspiksi
* . setup.sh

# Testing

* roscore
* rosrun araspiksi araspiksi.py --piksi_sim fixed_rtk --robosim --draw M.txt

Should spell the letter M.

# Setup de la VM

* Télécharger une image Ubuntu 14 [Image testée](http://releases.ubuntu.com/14.04.3/ubuntu-14.04.3-desktop-amd64.iso)
 
* Installer [VirtualBox](https://www.virtualbox.org/wiki/Linux_Downloads)
    * Télécharger la version pour Ubuntu 64bit (AMD64)
    * Dans le terminal: sudo dpkg -i nom_du_fichier.deb
    * Dans le terminal: sudo apt-get install dkms (pour pouvoir installer Guest Additions pour avoir la bonne taille de l'écran dans la VM)
    * Installer l'[extension pack](https://www.virtualbox.org/wiki/Downloads) (dans Virtualbox)

* Pour créer la VM:
    * New
    * Type : Linux, Version : Ubuntu 64bit
    * Next, Next, vmdk (si on veut une VM portable), Next, Next, Create (choisir plus de 10GO de stockage)
    * Clique droit -> Settings
    * Storage : Controller: IDE -> Empty choisir l'ISO téléchargé
    * Lancer la VM, installer Ubuntu
    * Clique droit + Settings
    * System : décocher Floppy et Optical
    * Pour pouvoir utiliser les USB dans la VM : sudo adduser YOURUSERNAME vboxusers [raison](http://stackoverflow.com/questions/20021300/usb-devices-are-not-recognized-in-virtualbox-linux-host)
    * Clique droit + Settings -> USB -> deuxième bouton sur la droite (+ vert) et selectionner le Piksi
    * Lancer la VM, et après qu'Ubuntu est lancé, dans la barre Menu : Devices -> Insert Guest Additions… -> OK

